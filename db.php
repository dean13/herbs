<?php
//nawiązanie połączenia z bazą danych
function getDB() {
	$db = new pdo('mysql:host=localhost;dbname=herbs', 'root', '');
	$db->exec("set names utf8");
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	return $db;
}

//pobranie id, nazw i łacińskich nazw ziół
function getHerbsNames() {
	$result = getDB()->query('SELECT id,name, name_latin FROM herbs WHERE id <> 1 ORDER BY name ASC');
	return $result;
}

//pobranie ziół
function getHerbs() {
	$result = getDB()->query('SELECT * FROM herbs WHERE id <> 1 ORDER BY name ASC');
	return $result;
}

//pobranie markerów ziół
function getMarkers() {
	$result = getDB()->query('SELECT id,lat,lng,name,description FROM markers');
	return $result;
}

//pobranie markerów dla konkretnego zioła
function getHerbMarkers($id) {
	$result = getDB()->prepare('SELECT id,lat,lng,name,description FROM markers WHERE id_herb = :id');
	$result->bindValue(':id', $id, PDO::PARAM_STR);
	$result->execute();
	return $result;
}

//fpobranie danych użytkownika o podanym loginie
function getUserData($login) {
	$result = getDB()->prepare('SELECT * FROM users WHERE login = :login LIMIT 1');
	$result->bindValue(':login', $login, PDO::PARAM_STR);
	$result->execute();
	return $result->fetch();
}

//fpobranie danych użytkownika o podanym id
function getUserDataByID($id) {
	$result = getDB()->prepare('SELECT * FROM users WHERE id = :id LIMIT 1');
	$result->bindValue(':id', $id, PDO::PARAM_INT);
	$result->execute();
	return $result->fetchObject();
}

//sprawdzenie poprawności danych logowania
function login($login, $pass) {
	$result = getDB()->prepare('SELECT COUNT(*) FROM users WHERE login = :login AND password = :pass LIMIT 1');
	$result->bindValue(':login', $login, PDO::PARAM_STR);
	$result->bindValue(':pass', $pass, PDO::PARAM_STR);
	$result->execute();
	return $result->fetchColumn();
}

//pobranie danych markera wystąpienia o podanym id
function getMarkerInfo($id) {
	$result = getDB()->prepare('SELECT m.id, m.name, h.name as herb, name_latin as latin, m.description, h.id as h_id,date FROM herbs h, markers m, users u WHERE m.id_user = u.id AND m.id_herb = h.id AND m.id= :id LIMIT 1');
	$result->bindValue(':id', $id, PDO::PARAM_STR);
	$result->execute();
	return $result;
}

//sprawdzenie czy zioło już istnieje (łacińska nazwa)
function checkHerb($latinName) {
	$result = getDB()->prepare('SELECT COUNT(*) FROM herbs WHERE name_latin = :name');
	$result->bindValue(':name', $latinName, PDO::PARAM_STR);
	$result->execute();
	return $result->fetchColumn();
}

//sprawdzenie czy login jest zajęcty
function checkLogin($login) {
	$result = getDB()->prepare('SELECT COUNT(*) FROM users WHERE login = :login');
	$result->bindValue(':login', $login, PDO::PARAM_STR);
	$result->execute();
	return $result->fetchColumn();
}

//dodaje użytkownika
function register($login, $password) {
	$result = getDB()->prepare("INSERT INTO users (login, password) VALUES (:login, :password)");
	$result->bindValue(':login', $login, PDO::PARAM_STR);
	$result->bindValue(':password', $password, PDO::PARAM_STR);
	$result->execute();
	$count = $result->rowCount();
	return $count;
}

//dodaje zioło
function newHerb($name, $nameLatin, $description) {
	$result = getDB()->prepare("INSERT INTO herbs (name, name_latin, description) VALUES ( :name, :nameLatin, :description)");
	$result->bindValue(':name', $name, PDO::PARAM_STR);
	$result->bindValue(':nameLatin', $nameLatin, PDO::PARAM_STR);
	$result->bindValue(':description', $description, PDO::PARAM_STR);
	$result->execute();
	$count = $result->rowCount();
	return $count;
}

//dodaje zioło ze zdjęciem
function newHerbImg($name, $nameLatin, $description, $img) {
	$result = getDB()->prepare("INSERT INTO herbs (name, name_latin, description, img) VALUES ( :name, :nameLatin, :description, :img)");
	$result->bindValue(':name', $name, PDO::PARAM_STR);
	$result->bindValue(':nameLatin', $nameLatin, PDO::PARAM_STR);
	$result->bindValue(':description', $description, PDO::PARAM_STR);
	$result->bindValue(':img', $img, PDO::PARAM_STR);
	$result->execute();
	$count = $result->rowCount();
	return $count;
}

//dodaje wystąpienie
function newMarker($herb, $name, $lat, $lng, $description) {
	$result = getDB()->prepare("INSERT INTO markers (id_herb, id_user, date, name, lat, lng, description) VALUES (:herb, :user, CURDATE(), :name, :lat, :lng, :description)");
	$result->bindValue(':herb', $herb, PDO::PARAM_INT);
	$result->bindValue(':user', $_SESSION['user_id'], PDO::PARAM_INT);
	$result->bindValue(':name', $name, PDO::PARAM_STR);
	$result->bindValue(':lat', $lat, PDO::PARAM_STR);
	$result->bindValue(':lng', $lng, PDO::PARAM_STR);
	$result->bindValue(':description', $description, PDO::PARAM_STR);
	$result->execute();
	$count = $result->rowCount();
	return $count;
}

//usuwa wystąpienie
function deleteMarker($id) {
	$result = getDB()->prepare('DELETE FROM markers WHERE id = :id');
	$result->bindParam(':id', $id, PDO::PARAM_INT);
	$result->execute();
	$count = $result->rowCount();
	return $count;
}

function getHerbDataWithLocalization($id) {
	$result = getDB()->prepare('SELECT herbs.name as h_name, herbs.description as h_description, markers.name as destination, markers.description as m_description, herbs.name_latin, herbs.img
FROM herbs JOIN markers ON markers.id_herb=herbs.id WHERE herbs.id = :id');
	$result->bindValue(':id', $id, PDO::PARAM_INT);
	$result->execute();
	return $result->fetchObject();
}

function getHerbData($id) {
	$result = getDB()->prepare('SELECT * FROM herbs WHERE id = :id');
	$result->bindValue(':id', $id, PDO::PARAM_INT);
	$result->execute();
	return $result->fetchObject();
}
?>

