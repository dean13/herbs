<div id="container">
	<div id="header">
		<?php
		include 'header.php';
		$herbs = getHerbs();
		?>
	</div>
	<div id="body">
		<div class="breadcrumbs breadcrumbs-dark">
			<div class="container">
				<h1 class="pull-left">Atlas ziół</h1>
				<ul class="pull-right breadcrumb">
					<li><a href="index.php">Strona główna</a></li>
					<li class="active">Atlas ziół</li>
				</ul>
			</div><!--/container-->
		</div>

		<div class="container">

			<table class="table table-hover">
				<thead>
				<tr>
					<th>Nazwa</th>
					<th>Nazwa łac.</th>
					<th>Zdjęcie</th>
					<th>Opis</th>
				</tr>
				</thead>
				<?php
				foreach ($herbs as $herb) {
					echo '<tr><td class="col-md-2"><a href="index.php?herb='.$herb['id'].'"">'.$herb['name'].'</a></td><td class="col-md-2">'.$herb['name_latin'].'</td>';
					if (!empty($herb['img'])) {
						echo '<td class="col-md3"><img src="img/'.$herb['img'].'" height="200px"></td>';
					} else {
						echo '<td class="col-md-3">brak zdjęcia</td>';
					}
					echo '<td class="col-md-7">'.$herb['description'].'</td></tr>';
				}
				?>
			</table>
		</div>
	</div>
	<div id="footer">
		<?php
		include 'footer.php';
		?>
	</div>
</div>



