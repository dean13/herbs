var mapa;
var submitDeletePlace = $("#submit-delete-place");
var markers = [];
var selectedMarker = null;
var infoWindow = new google.maps.InfoWindow(); 
var geokoder = new google.maps.Geocoder();
var opcje_markera = {draggable : true, animation: google.maps.Animation.DROP}
var rozmiar = new google.maps.Size(32,38);
var rozmiar_cien = new google.maps.Size(59,32);
var punkt_startowy = new google.maps.Point(0,0);
var punkt_zaczepienia = new google.maps.Point(16,38);

var wskaznik = new google.maps.Marker(opcje_markera);

google.maps.event.addDomListener(window, 'load', mapStart);

google.maps.event.addListener(wskaznik, 'dragend', function() { 
	$("#place-lat").val(wskaznik.getPosition().lat());
	$("#place-lng").val(wskaznik.getPosition().lng());
});

function getUrlParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
} 

function mapStart()  {  
	var opcjeMapy = {center: new google.maps.LatLng( 50.033611, 22.004722), zoom: 15, mapTypeId: google.maps.MapTypeId.TERRAIN, panControl:false, zoomControl: true, scaleControl:true,
    zoomControlOptions: {
        style: google.maps.ZoomControlStyle.LARGE,
        position: google.maps.ControlPosition.RIGHT_TOP
    }}
		mapa = new google.maps.Map(document.getElementById("mapka"), opcjeMapy);
		
		if(navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function(position) {
			var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
			var infowindow = new google.maps.InfoWindow({
				map: mapa,
				position: pos,
				content: 'Wykryta lokalizacja'
			});
			mapa.setCenter(pos);
			});
		}
		
		if(getUrlParameter('herb')) {
			setHerbMarkers(getUrlParameter('herb'));
		} else {
			setMarkers();	
		}
}

function clearOverlays() {
	for (var i = 0; i < markers.length; i++ ) {
		markers[i].setMap(null);
	}
	markers.length = 0;
}
	
function setMarkers() {
	clearOverlays();
	$.getJSON("json.php", "markers=true", function(data) {
		$.each( data, function( key, val ) {
			dodajMarker(val.lat,val.lng,val.id);
		});
	});
}

function setHerbMarkers(id) {
	clearOverlays();
	$.getJSON("json.php", {'herb-markers' : id}, function(data) {
		$.each( data, function( key, val ) {
			dodajMarker(val.lat,val.lng,val.id);
		});
	});
}

function dodajMarker(lat,lng,id) {
	var opcjeMarkera =   
		{  
			position: new google.maps.LatLng(lat,lng), 
			icon:  new google.maps.MarkerImage("img/herb.png", rozmiar, punkt_startowy, punkt_zaczepienia),
			animation: google.maps.Animation.DROP,
			map: mapa,
			id : id
		}  
	var marker = new google.maps.Marker(opcjeMarkera);

	google.maps.event.addListener(marker,"click",function() {	
		selectedMarker = marker;
		$.getJSON("json.php", { "marker-info": id }, function(data) {
			marker.txt = '<div class="info-window"><h5>'+data[0].name+'</h5><a href="index.php?herb='+data[0].h_id+'"><ins>'+data[0].herb+' (<i>'+data[0].latin+'</i>)</ins></a><br>'+(data[0].description?data[0].description+'<br>':'')+'Dodano : '+data[0].date+' <a class="btn btn-default btn-sm" href="index.php?action=delete&id='+data[0].id+'"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Usuń</a></div>';
			infoWindow.setContent(marker.txt);
			infoWindow.open(mapa,marker);
		});
		mapa.panTo(marker.getPosition());
	});
	markers.push(marker);
	return marker;
}

function skoczDoAdresu(adres) {
	geokoder.geocode({address: adres}, function(wyniki, status) {
		if(status == google.maps.GeocoderStatus.OK) {
			mapa.setCenter(wyniki[0].geometry.location);
			if (wyniki[0].geometry.viewport)
				mapa.fitBounds(wyniki[0].geometry.viewport);
		} else {
			alert("brak wyników!");
		}
	});
}

function setMarker(loc) {
	wskaznik.setMap(null);
	wskaznik.setPosition(loc);
	wskaznik.setMap(mapa);
	$("#place-lat").val(loc.lat());
	$("#place-lng").val(loc.lng());
}

$("#place-button").click(function(event) {
	event.preventDefault();
	setMarker(mapa.getCenter());
	$(this).hide();
	$("#place-box").show();
});



$("#delete-place-form").submit(function(event) {
	event.preventDefault();
	preloader.show();
	submitDeletePlace.attr('disabled','disabled');
	$.ajax({
		type: "POST",
		url: "form_process.php",
		data: {
			action: "delete-marker",
			id : selectedMarker.id
		},
		success: function(ret) {
			if(ret == 1) {
				deletePlaceInfo.text("Usunięto!").show().delay(1000).fadeOut(500, function() {
					closeEditPlaceBox();
					if(mapState == 1) {
						setPlaceMarkers();
					}
					if(mapState == 2) {
						setUnauthorisedPlaceMarkers();
					}
				});
			} else {
				deletePlaceInfo.text(ret).show().delay(1000).fadeOut(500, function() {
					
				});
			}	
			preloader.hide();
			submitDeletePlace.removeAttr('disabled');
		}
	});
});