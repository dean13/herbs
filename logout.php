<?php
session_start();
$_SESSION['logged'] = false;
$_SESSION['user_id'] = -1;
$_SESSION['admin'] = false;
$_SESSION['banned'] = false;
session_destroy();
header('Location: index.php');
?>