<div id="container">
	<div id="header">
		<?php
		include 'header.php';
		if(isset($_POST['submit-marker'])) {
			if (empty($_POST['place-name']) || empty($_POST['place-description']) || empty($_POST['place-herb']) || empty($_POST['place-lat']) || empty($_POST['place-lng'])) {
				?>
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					Uzupełnij puste pola!
				</div>
				<?php
			} else {
				if(strlen($_POST['place-name']) > 45 || strlen($_POST['place-description']) > 200) {
					?>
					<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Niepoprawny format danych!
					</div>
					<?php
				} else {
					$result = newMarker($_POST['place-herb'], $_POST['place-name'], $_POST['place-lat'], $_POST['place-lng'],  $_POST['place-description']);
					if($result != 1) {
						?>
						<div class="alert alert-danger alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							Nieznany błąd!
						</div>
						<?php
					} else {
						?>
						<div class="alert alert-success alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							Dodano nowe wsytąpinie! Zaraz nastąpi przekierowanie na stronę główną!
						</div>
						<?php
						header('Location: index.php');
					}
				}
			}
		}

		if(isset($_GET['action'])) {
			if($_GET['action'] == 'new_marker') {
				if($_SESSION['logged']) {
					?>
					<div  id="new-marker" class="panel panel-primary">
						<div class="panel-heading">Nowe wystąpienie</div>
						<div class="panel-body">
							<form action="" method="post" id="place-form">
								<div class="form-group">
									<label for="place-name">Nazwa</label>
									<input type="text" class="form-control" name="place-name" id="place-name" placeholder="Pobliski obiekt lub miejscowość" pattern=".{3,}" required>
								</div>
								<div class="form-group">
									<label for="place-herb">Wybierz zioło</label>
									<select name="place-herb" id="place-herb" class="form-control">
										<?php
										$herbs = getHerbsNames() ;
										foreach ($herbs as $herb) {
											echo '<option value="'.$herb['id'].'">'.$herb[name].'('.$herb['name_latin'].')</option>';
										}
										?>
										<option value="1">Nieokreślone</option>
									</select>
								</div>
								<div class="form-group">
									<label for="place-description">Opis</label>
									<textarea class="form-control" rows="8" name="place-description" id="place-description" placeholder="Cechy charakterystyczne, wskazówki jak znaleźć zioła"></textarea>
								</div>
								<div class="form-group" id="place-button">
									<a class="btn btn-default" id="add-maker">Dodaj lokalizację</a>
								</div>
								<div class="form-group" id="place-box">
									<label for="place-description">Przesuń marker w miejsce wystąpienia</label>
									<input type="text" class="form-control" name="place-lat" id="place-lat" >
									<input type="text" class="form-control" name="place-lng" id="place-lng" >
								</div>
								<div class="inline">
									<button type="submit" class="btn btn-primary" name="submit-marker" id="submit-marker">Zapisz</button>
									<button type="button" class="btn btn-default" name="close" id="close-marker">Zamknij</button>
								</div>
							</form>
						</div>
					</div>
					<?php
				}
			}
			if($_GET['action'] == 'delete' && isset($_GET['id'])) {
				if($_SESSION['logged'] && $_SESSION['admin']) {
					deleteMarker($_GET['id']);
					header('Location: index.php');
				} else {
					?>
					<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Brak uprawnień!
					</div>
					<?php
				}
			}
		}
		?>
	</div>
	<div id="body">
		<div class="breadcrumbs breadcrumbs-dark">
			<div class="container">
				<h1 class="pull-left">Zioła lecznicze</h1>
				<ul class="pull-right breadcrumb">
					<li><a href="index.php">Strona główna</a></li>
					<li class="active">Zioła lecznicze</li>
				</ul>
			</div><!--/container-->
		</div>

		<div class="container">
				<?php
				if(isset($_GET['herb'])) {
					$result = getHerbDataWithLocalization($_GET['herb']);
					if($result != null) {
						echo '<div class="page-header">';
						echo '<h1>'.$result->h_name.' <small>('.$result->name_latin.')</small></h1>';
						echo '<p>'.$result->h_description.'</p>';
						echo '<h4>Występowanie</h4><p>'.$result->destination.'</p>';
						echo '<h4>Jak trafić</h4><p>'.$result->m_description.'</p>';
						echo '<img id="fot" src="img/'.$result->img.'">';
						echo '</div>';
					}
					else {
						$result = getHerbData($_GET['herb']);
						echo '<div class="page-header">';
						echo '<h1>'.$result->name.' <small>('.$result->name_latin.')</small></h1>';
						echo '<p>'.$result->description.'</p>';
						echo '<h4>Występowanie</h4><p>Nie dodano występowania</p>';
						echo '<h4>Jak trafić</h4><p>Nie ma jeszcze opisu lokalizacji</p>';
						echo '<img id="fot" src="img/'.$result->img.'">';
						echo '</div>';
					}
				} else {
					echo '<div class="jumbotron"><h2>Witaj na stronie Zioła lecznicze!</h2>
					<p>Jeśli chcesz dowiedzieć się wszystkeigo o różnych rodzajach ziół, przejdź do sekcji Atlas ziół !</p>
  					<p><a class="btn btn-success btn-lg" href="herbs.php" role="button">Atlas ziół</a></p>
					</div>';
				}
				?>
			<div id="content">
				<div class="page-header">
					<h3>Mapka wystąpień</h3>
				</div>
				<div id="mapka"></div>
			</div>
		</div>
	</div>
	<div id="footer">
		<?php
		include 'footer.php';
		?>
	</div>
</div>


<script>
	$(document).ready(function() {
		$('#close-marker').click('on',function() {
			$('.panel').hide();
		});
	});
</script>

