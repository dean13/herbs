<?php
session_start();
include 'db.php';
header('Content-Type: application/json');
if(isSet($_GET["markers"])) {
	echo json_encode(getMarkers()->fetchAll(( PDO::FETCH_ASSOC )));
}
if(isSet($_GET["herb-markers"])) {
	echo json_encode(getHerbMarkers($_GET["herb-markers"])->fetchAll(( PDO::FETCH_ASSOC )));
}
if(isSet($_GET["marker-info"])) {
	echo json_encode(getMarkerInfo($_GET["marker-info"])->fetchAll(( PDO::FETCH_ASSOC )));
}
?>