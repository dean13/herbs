<script src="js/jquery-2.1.3.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/script.js"></script>

<div class="footer-v1">
    <div class="footer">
        <div class="container">
            <div class="row" style="margin-right: 0">
                <!-- About -->
                <div class="col-md-7 md-margin-bottom-40">
                    <p style="color: #bbb">
                        Jeśli jesteś gościem i nie posiadasz konta w tym serwisie możesz przeglądać atlas ziół oraz najnowsze dodane zioła.
                        <br>Osoby zarejestrowna ponadto mogą dodać nowe wystąpenie danego zioła oraz wyszukiwać zioła w danej miejscowości.
                        <br>Jeżli jesteś administratorem wykorzystasz w pełni funkcionalność strony
                        dodając nowe zioła.
                    </p>
                </div><!--/col-md-6-->
                <img src="img/logo.png">
                <!-- End About -->
            </div>
        </div>
    </div>

</div>

</body>
</html>