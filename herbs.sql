CREATE DATABASE  IF NOT EXISTS `herbs` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `herbs`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: herbs
-- ------------------------------------------------------
-- Server version	5.5.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `herbs`
--

DROP TABLE IF EXISTS `herbs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `herbs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `name_latin` varchar(80) NOT NULL,
  `description` varchar(400) NOT NULL,
  `img` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  UNIQUE KEY `name_latin_UNIQUE` (`name_latin`),
  UNIQUE KEY `img_UNIQUE` (`img`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `herbs`
--

LOCK TABLES `herbs` WRITE;
/*!40000 ALTER TABLE `herbs` DISABLE KEYS */;
INSERT INTO `herbs` VALUES (1,'Nieokreślone','-','Nierozpoznane przez użytkownika zioła.',NULL),(2,'Melisa lekarska','Melissa officinalis L.','Łodyga czterokanciasta, miękko owłosiona, dość silnie rozgałęziona. Liście sercowate, osadzone na ogonku, brzeg karbowano – ząbkowany. Drobne, dwuwargowe kwiaty osadzone w nibyokółkach tworzą szczytowe kwiatostany (osadzone są w pachwinach górnych liści). Owoc: rozłupnia. Posiada charakterystyczny, słodkawy zapach cytryny. Liście zawierają olejek znany jako Oleum Melissae.',NULL),(3,'Babka lancetowata','Plantago lanceolata L.','Łodyga bezlistna, głęboko bruzdkowana, zakończona kulistym lub jajowatym kwiatostanem długości 1 - 3 cm. Liście lancetowate lub równowąsko lancetowate, całobrzegie, dołem zwężające się w ogonek, unerwienie równoległe, tworzą dołem różyczkę. Kwiaty drobne, nitki pręcików dwa razy dłuższe od korony. Owoc: dwunasienna torebka.','44853008.jpg'),(4,'Chaber bławatek','Centaurea cyanus L.','Łodyga prosta, rozgałęziona w górnej części. Liście osadzone naprzemianlegle, równowąsko lancetowate, szarozielone. Kwiatostany w postaci koszyczków wyrastających pojedynczo na szczytach pędów o średnicy 2 - 3 cm.. Wewnątrz koszyczka znajdują się rurkowate, fioletowe kwiaty generatywne natomiast niebieskie kwiaty brzeżne, kształtu lejkowatego są bezpłciowe. Owocem jest niełupka.',NULL),(9,'Krwawnik pospolity','Achillea millefolium','Roślina trwała, wysokości 10-60 cm. Łodygi dość liczne, wzniesione, rozgałęzione w górnej części, owłosione. Liście lancetowate, ciemnozielone, dwu, trzykrotnie pierzastosieczne. Koszyczki liczne, drobne, skupione w płaskie baldachogrona.','krwawnik-pospolity_16981282815246.jpg');
/*!40000 ALTER TABLE `herbs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `markers`
--

DROP TABLE IF EXISTS `markers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `markers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_herb` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `date` date NOT NULL,
  `name` varchar(80) NOT NULL,
  `lat` float(10,6) NOT NULL,
  `lng` float(10,6) NOT NULL,
  `description` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `markers_users_idx` (`id_user`),
  KEY `markers_herbs_idx` (`id_herb`),
  CONSTRAINT `markers_herbs` FOREIGN KEY (`id_herb`) REFERENCES `herbs` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `markers_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `markers`
--

LOCK TABLES `markers` WRITE;
/*!40000 ALTER TABLE `markers` DISABLE KEYS */;
INSERT INTO `markers` VALUES (1,2,1,'2015-06-10','Okolice Wisłoka',50.049763,22.014420,'Zejść pod most'),(2,3,1,'2015-06-10','Okolice Żwirowni',50.010601,22.000772,'Śladowe ilości w haszczach');
/*!40000 ALTER TABLE `markers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(24) NOT NULL,
  `password` varchar(80) NOT NULL,
  `admin` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `login_UNIQUE` (`login`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','admin',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-06-13 15:38:59
