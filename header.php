<?php
session_start();
include 'db.php';
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Występowanie ziół</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<script src="http://maps.google.com/maps/api/js?sensor=true" type="text/javascript"></script>
</head>
<body>
<?php
if (isset($_SESSION['logged'])) {
?>
    <nav class="navbar navbar-default navbar-static-top">
		<div class="container">
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-logo" id="logo">
					<li><a href="index.php"><img src="img/logo.png"></a> </li>
				</ul>

				<div id="menu">
					<ul class="nav navbar-nav navbar-menu">
						<li id="geolocator-li">
							<form id="geolocator" action="#" onsubmit="skoczDoAdresu(document.getElementById('szukanyAdres').value); return false;" class="form-inline">
								<input type="text" class="form-control" id="szukanyAdres" placeholder="Nazwa lub adres"/>
								<a href="index.php"><input type="submit" class="btn btn-default" value="Mapa" /></a>
							</form>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Nowe zioła<span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<?php
								$herbs = getHerbsNames() ;
								foreach ($herbs as $herb) {
									echo '<li><a href="index.php?herb='.$herb['id'].'">'.$herb['name'].'('.$herb['name_latin'].')</a></li>';
								}
								?>
							</ul>
						</li>
						<li><a href="herbs.php">Atlas ziół</a></li>
						<li id="menu-add-marker"><a href="index.php?action=new_marker">Dodaj wystąpienie</a></li>
						<?php
						if (isset($_SESSION['admin'])) {
							?>
							<li><a href="add_herb.php">Dodaj roślinę</a></li>
						<?php } ?>
						<li><div id="preloader"></div></li>
						<ul class="nav navbar-nav navbar-right">
							<li class="active">
								<a href="logout.php" style="text-align: center">Wyloguj<span class="sr-only">(current)</span>

								</a>
								<?php
									if(isset($_SESSION['admin'])) {
										echo '<div id="loged"><span class="label label-danger">Administrator</span>';
										$userData = getUserDataByID($_SESSION['user_id']);
										echo ' '.$userData->login;
										echo '</div>';
									}else {
										echo '<div id="loged"><span class="label label-success">Użytkownik</span>';
										$userData = getUserDataByID($_SESSION['user_id']);
										echo ' '.$userData->login;
										echo '</div>';
									}
								?>
							</li>
						</ul>
					</ul>

				</div>
			</div>
		</div>
    </nav>

	<?php
} else {
	?>
   <nav class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav navbar-logo" id="logo">
				<li><a href="index.php"><img src="img/logo.png"></a> </li>
			</ul>
			<div id="menu">
				<ul class="nav navbar-nav navbar-menu">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Nowe zioła<span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<?php
							$herbs = getHerbsNames() ;
							foreach ($herbs as $herb) {
								echo '<li><a href="index.php?herb='.$herb['id'].'">'.$herb['name'].'('.$herb['name_latin'].')</a></li>';
							}
							?>
						</ul>
					</li>
					<li><a href="herbs.php">Atlas ziół</a></li>
					<ul class="nav navbar-nav navbar-right">
						<li><a href="register.php">Zarejestruj</a></li>
						<li class="active"><a href="login.php">Zaloguj<span class="sr-only">(current)</span></a></li>
					</ul>
				</ul>

			</div>
        </div>
      </div>
    </nav>
<?php } ?>
