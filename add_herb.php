<?php
include 'header.php';
if(isset($_POST['submit'])) {
	if (empty($_POST['herb-name']) || empty($_POST['herb-latin-name']) || empty($_POST['herb-description'])) {
?>
<div class="alert alert-danger alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  Uzupełnij puste pola!
</div>
<?php
	} else {
		if(strlen($_POST['herb-name']) > 45 || strlen($_POST['herb-latin-name']) > 80 || strlen($_POST['herb-description']) > 400) {
		?>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			Niepoprawny format danych!
		</div>
		<?php	
		} else {
			$result = checkHerb($_POST['herb-latin-name']);
			if($result != 1) {
				if(empty($_FILES['herb-img']['name'])) {
					$newHerbResult = newHerb($_POST['herb-name'], $_POST['herb-latin-name'], $_POST['herb-description']);
				} else {
					$newHerbResult = newHerbImg($_POST['herb-name'], $_POST['herb-latin-name'], $_POST['herb-description'], $_FILES['herb-img']['name']);
				}
				if($newHerbResult != 1) {
				?>
					<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Nieznany błąd!
					</div>
				<?php	
				} else {
					$max_rozmiar = 800*800;
					if (is_uploaded_file($_FILES['herb-img']['tmp_name'])) {
						if ($_FILES['herb-img']['size'] > $max_rozmiar) {
					?>
					<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Rozmiar pliku jest zbyt duży!
					</div>
					<?php
						} else {
							move_uploaded_file($_FILES['herb-img']['tmp_name'], './img/'.$_FILES['herb-img']['name']);
							?>
							<div class="alert alert-success alert-dismissable">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								Dodano nową roślinę! Zaraz nastąpi przekierowanie do atlasu ziół!
							</div>
							<?php
								header('Location: herbs.php');
						}
					} else {
					 ?>
					<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Błąd podczas wysyłania pliku!
					</div>
					<?php
					}
				}
			} else {
			?>
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				Zioło już istnieje!
			</div>
			<?php
			}
		}
	}
}
?>
<div class="container" id="container">
	<form action="" method="post" id="herb-form" enctype="multipart/form-data">
		<div class="form-group">
			<label for="herb-name">Nazwa</label>
			<input type="text" name="herb-name" id="herb-name" class="form-control" placeholder="(max 45)" maxlength="45" required autofocus>
		</div>
		<div class="form-group">
			<label for="herb-latin-name">Nazwa łacińska</label>
			<input type="text" name="herb-latin-name" id="herb-latin-name" class="form-control" placeholder="(max 80)" maxlength="80" required>
		</div>
		<div class="form-group">
			<label for="herb-description">Opis</label>
			<textarea name="herb-description" id="herb-description" class="form-control" rows="5" placeholder="(max 400)" maxlength="400" required></textarea>
		</div>
		<div class="form-group">
			<label for="herb-img">Zdjęcie (niewymagane)</label>
			<input type="file" id="herb-img" name="herb-img" accept="image/x-png, image/gif, image/jpeg">
		</div>
		<div class="form-group">
			<input  type="submit" name="submit" class="btn btn-lg btn-primary btn-block" value="Dodaj roślinę">
		</div>
	</form>
</div>
<?php
include 'footer.php';
?>