<?php
include 'header.php';
if(isset($_POST['submit'])) {
	if (empty($_POST['login-login']) || empty($_POST['login-password'])) {
?>
<div class="alert alert-danger alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  Uzupełnij puste pola!
</div>
<?php
	} else {
		$result = login($_POST['login-login'], $_POST['login-password']);
		if($result == 1) {
		?>
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			Zalogowano! Zaraz nastąpi przekierowanie na stronę główną!
		</div>
		<?php
			$userData = getUserData($_POST['login-login']);
			$_SESSION['logged'] = true;
			$_SESSION['user_id'] = $userData['id'];
			$_SESSION['user_login'] = $userData['login'];
			if($userData['admin'] == 1)
				$_SESSION['admin'] = true;
			header('Location: index.php');
		}
		?>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			Niepoprawny login i/lub hasło!
		</div>
		<?php
	}
}
?>
<div id="container">
	<form action="" method="post" id="login-form">
		<div class="form-group">
			<label for="login-login" class="sr-only">Login</label>
			<input type="text"  name="login-login" id="login-login" class="form-control" placeholder="Login" required autofocus>
		</div>
		<div class="form-group">
			<label for="login-password" class="sr-only">Hasło</label>
			<input type="password"name="login-password" id="login-password" class="form-control" placeholder="Hasło" required>
		</div>
		<input type="submit" name="submit" class="btn btn-lg btn-primary btn-block" value="Zaloguj">
	</form>
</div>
<?php
include 'footer.php';
?>