<?php
include 'header.php';
if(isset($_POST['submit'])) {
	if (empty($_POST['register-login']) || empty($_POST['register-password']) || empty($_POST['register-password2'])) {
?>
<div class="alert alert-danger alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  Uzupełnij puste pola!
</div>
<?php
	} else {
		if($_POST['register-password'] != $_POST['register-password2']) {
		?>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			Hasła są różne!
		</div>
		<?php
		} else {
			$result = checkLogin($_POST['register-login']);
			if($result != 1) {
				$registerResult = register($_POST['register-login'],$_POST['register-password']);
				if($registerResult != 1) {
				?>
					<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						Nieznany błąd!
					</div>
				<?php
				} else {
				?>
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					Zalogowano! Zaraz nastąpi przekierowanie na stronę główną!
				</div>
				<?php
					$userData = getUserData($_POST['register-login']);
					$_SESSION['logged'] = true;
					$_SESSION['user_id'] = $userData['id'];
					header('Location: index.php');
				}
			} else {
			?>
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				Login jest zajęty!
			</div>
			<?php
			}
		}
	}
}
?>
<div class="container" id="container">
	<form action="" method="post" id="register-form">
		<div class="form-group">
			<label for="register-login" class="sr-only">Login</label>
			<input type="text" name="register-login" id="register-login" class="form-control" placeholder="Login" maxlength="24" required autofocus>
		</div>
		<div class="form-group">
			<label for="register-password" class="sr-only">Hasło</label>
			<input type="password" name="register-password" id="register-password" class="form-control" placeholder="Hasło" maxlength="16" required>
		</div>
		<div class="form-group">
			<label for="register-password2" class="sr-only">Powtórz hasło</label>
			<input type="password" name="register-password2" id="register-password2" class="form-control" placeholder="Powtórz hasło" maxlength="16" required>
		</div>
		<div class="form-group">
			<input  type="submit" name="submit" class="btn btn-lg btn-primary btn-block" value="Zarejestruj">
		</div>
	</form>
</div>
<?php
include 'footer.php';
?>